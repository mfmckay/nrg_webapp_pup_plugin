/*
 * GENERATED FILE
 * Created on Tue Jan 17 11:00:55 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function rad_radiologyReadData(){
this.xsiType="rad:radiologyReadData";

	this.getSchemaElementName=function(){
		return "radiologyReadData";
	}

	this.getFullSchemaElementName=function(){
		return "rad:radiologyReadData";
	}
this.extension=dynamicJSLoad('xnat_mrAssessorData','generated/xnat_mrAssessorData.js');

	this.Modality=null;


	function getModality() {
		return this.Modality;
	}
	this.getModality=getModality;


	function setModality(v){
		this.Modality=v;
	}
	this.setModality=setModality;

	this.Reader=null;


	function getReader() {
		return this.Reader;
	}
	this.getReader=getReader;


	function setReader(v){
		this.Reader=v;
	}
	this.setReader=setReader;

	this.Technique=null;


	function getTechnique() {
		return this.Technique;
	}
	this.getTechnique=getTechnique;


	function setTechnique(v){
		this.Technique=v;
	}
	this.setTechnique=setTechnique;

	this.Finding=null;


	function getFinding() {
		return this.Finding;
	}
	this.getFinding=getFinding;


	function setFinding(v){
		this.Finding=v;
	}
	this.setFinding=setFinding;

	this.Finding_normalStatus=null;


	function getFinding_normalStatus() {
		return this.Finding_normalStatus;
	}
	this.getFinding_normalStatus=getFinding_normalStatus;


	function setFinding_normalStatus(v){
		this.Finding_normalStatus=v;
	}
	this.setFinding_normalStatus=setFinding_normalStatus;


	this.isFinding_normalStatus=function(defaultValue) {
		if(this.Finding_normalStatus==null)return defaultValue;
		if(this.Finding_normalStatus=="1" || this.Finding_normalStatus==true)return true;
		return false;
	}

	this.Diagnosis=null;


	function getDiagnosis() {
		return this.Diagnosis;
	}
	this.getDiagnosis=getDiagnosis;


	function setDiagnosis(v){
		this.Diagnosis=v;
	}
	this.setDiagnosis=setDiagnosis;

	this.HippocampalAtrophy=null;


	function getHippocampalAtrophy() {
		return this.HippocampalAtrophy;
	}
	this.getHippocampalAtrophy=getHippocampalAtrophy;


	function setHippocampalAtrophy(v){
		this.HippocampalAtrophy=v;
	}
	this.setHippocampalAtrophy=setHippocampalAtrophy;

	this.CorticalAtrophy=null;


	function getCorticalAtrophy() {
		return this.CorticalAtrophy;
	}
	this.getCorticalAtrophy=getCorticalAtrophy;


	function setCorticalAtrophy(v){
		this.CorticalAtrophy=v;
	}
	this.setCorticalAtrophy=setCorticalAtrophy;

	this.Leukoaraiosis=null;


	function getLeukoaraiosis() {
		return this.Leukoaraiosis;
	}
	this.getLeukoaraiosis=getLeukoaraiosis;


	function setLeukoaraiosis(v){
		this.Leukoaraiosis=v;
	}
	this.setLeukoaraiosis=setLeukoaraiosis;

	this.LargeInfarcts=null;


	function getLargeInfarcts() {
		return this.LargeInfarcts;
	}
	this.getLargeInfarcts=getLargeInfarcts;


	function setLargeInfarcts(v){
		this.LargeInfarcts=v;
	}
	this.setLargeInfarcts=setLargeInfarcts;

	this.SmallInfarcts=null;


	function getSmallInfarcts() {
		return this.SmallInfarcts;
	}
	this.getSmallInfarcts=getSmallInfarcts;


	function setSmallInfarcts(v){
		this.SmallInfarcts=v;
	}
	this.setSmallInfarcts=setSmallInfarcts;

	this.Microbleeds=null;


	function getMicrobleeds() {
		return this.Microbleeds;
	}
	this.getMicrobleeds=getMicrobleeds;


	function setMicrobleeds(v){
		this.Microbleeds=v;
	}
	this.setMicrobleeds=setMicrobleeds;

	this.SiteMicrobleeds=null;


	function getSiteMicrobleeds() {
		return this.SiteMicrobleeds;
	}
	this.getSiteMicrobleeds=getSiteMicrobleeds;


	function setSiteMicrobleeds(v){
		this.SiteMicrobleeds=v;
	}
	this.setSiteMicrobleeds=setSiteMicrobleeds;

	this.Lacunes=null;


	function getLacunes() {
		return this.Lacunes;
	}
	this.getLacunes=getLacunes;


	function setLacunes(v){
		this.Lacunes=v;
	}
	this.setLacunes=setLacunes;

	this.OtherSignificantFindings=null;


	function getOtherSignificantFindings() {
		return this.OtherSignificantFindings;
	}
	this.getOtherSignificantFindings=getOtherSignificantFindings;


	function setOtherSignificantFindings(v){
		this.OtherSignificantFindings=v;
	}
	this.setOtherSignificantFindings=setOtherSignificantFindings;


	this.isOtherSignificantFindings=function(defaultValue) {
		if(this.OtherSignificantFindings==null)return defaultValue;
		if(this.OtherSignificantFindings=="1" || this.OtherSignificantFindings==true)return true;
		return false;
	}

	this.AgingChanges=null;


	function getAgingChanges() {
		return this.AgingChanges;
	}
	this.getAgingChanges=getAgingChanges;


	function setAgingChanges(v){
		this.AgingChanges=v;
	}
	this.setAgingChanges=setAgingChanges;

	this.Comparison=null;


	function getComparison() {
		return this.Comparison;
	}
	this.getComparison=getComparison;


	function setComparison(v){
		this.Comparison=v;
	}
	this.setComparison=setComparison;

	this.History=null;


	function getHistory() {
		return this.History;
	}
	this.getHistory=getHistory;


	function setHistory(v){
		this.History=v;
	}
	this.setHistory=setHistory;

	this.Exam=null;


	function getExam() {
		return this.Exam;
	}
	this.getExam=getExam;


	function setExam(v){
		this.Exam=v;
	}
	this.setExam=setExam;

	this.FollowupRecommended=null;


	function getFollowupRecommended() {
		return this.FollowupRecommended;
	}
	this.getFollowupRecommended=getFollowupRecommended;


	function setFollowupRecommended(v){
		this.FollowupRecommended=v;
	}
	this.setFollowupRecommended=setFollowupRecommended;


	this.isFollowupRecommended=function(defaultValue) {
		if(this.FollowupRecommended==null)return defaultValue;
		if(this.FollowupRecommended=="1" || this.FollowupRecommended==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				return this.Mrassessordata ;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined)return this.Mrassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="modality"){
				return this.Modality ;
			} else 
			if(xmlPath=="reader"){
				return this.Reader ;
			} else 
			if(xmlPath=="technique"){
				return this.Technique ;
			} else 
			if(xmlPath=="finding"){
				return this.Finding ;
			} else 
			if(xmlPath=="finding/normal_status"){
				return this.Finding_normalStatus ;
			} else 
			if(xmlPath=="diagnosis"){
				return this.Diagnosis ;
			} else 
			if(xmlPath=="hippocampal_atrophy"){
				return this.HippocampalAtrophy ;
			} else 
			if(xmlPath=="cortical_atrophy"){
				return this.CorticalAtrophy ;
			} else 
			if(xmlPath=="leukoaraiosis"){
				return this.Leukoaraiosis ;
			} else 
			if(xmlPath=="large_infarcts"){
				return this.LargeInfarcts ;
			} else 
			if(xmlPath=="small_infarcts"){
				return this.SmallInfarcts ;
			} else 
			if(xmlPath=="microbleeds"){
				return this.Microbleeds ;
			} else 
			if(xmlPath=="site_microbleeds"){
				return this.SiteMicrobleeds ;
			} else 
			if(xmlPath=="lacunes"){
				return this.Lacunes ;
			} else 
			if(xmlPath=="other_significant_findings"){
				return this.OtherSignificantFindings ;
			} else 
			if(xmlPath=="aging_changes"){
				return this.AgingChanges ;
			} else 
			if(xmlPath=="comparison"){
				return this.Comparison ;
			} else 
			if(xmlPath=="history"){
				return this.History ;
			} else 
			if(xmlPath=="exam"){
				return this.Exam ;
			} else 
			if(xmlPath=="followup_recommended"){
				return this.FollowupRecommended ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				this.Mrassessordata=value;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined){
					this.Mrassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Mrassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Mrassessordata= instanciateObject("xnat:mrAssessorData");//omUtils.js
						}
						if(options && options.where)this.Mrassessordata.setProperty(options.where.field,options.where.value);
						this.Mrassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="modality"){
				this.Modality=value;
			} else 
			if(xmlPath=="reader"){
				this.Reader=value;
			} else 
			if(xmlPath=="technique"){
				this.Technique=value;
			} else 
			if(xmlPath=="finding"){
				this.Finding=value;
			} else 
			if(xmlPath=="finding/normal_status"){
				this.Finding_normalStatus=value;
			} else 
			if(xmlPath=="diagnosis"){
				this.Diagnosis=value;
			} else 
			if(xmlPath=="hippocampal_atrophy"){
				this.HippocampalAtrophy=value;
			} else 
			if(xmlPath=="cortical_atrophy"){
				this.CorticalAtrophy=value;
			} else 
			if(xmlPath=="leukoaraiosis"){
				this.Leukoaraiosis=value;
			} else 
			if(xmlPath=="large_infarcts"){
				this.LargeInfarcts=value;
			} else 
			if(xmlPath=="small_infarcts"){
				this.SmallInfarcts=value;
			} else 
			if(xmlPath=="microbleeds"){
				this.Microbleeds=value;
			} else 
			if(xmlPath=="site_microbleeds"){
				this.SiteMicrobleeds=value;
			} else 
			if(xmlPath=="lacunes"){
				this.Lacunes=value;
			} else 
			if(xmlPath=="other_significant_findings"){
				this.OtherSignificantFindings=value;
			} else 
			if(xmlPath=="aging_changes"){
				this.AgingChanges=value;
			} else 
			if(xmlPath=="comparison"){
				this.Comparison=value;
			} else 
			if(xmlPath=="history"){
				this.History=value;
			} else 
			if(xmlPath=="exam"){
				this.Exam=value;
			} else 
			if(xmlPath=="followup_recommended"){
				this.FollowupRecommended=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="modality"){
			return "field_data";
		}else if (xmlPath=="reader"){
			return "field_LONG_DATA";
		}else if (xmlPath=="technique"){
			return "field_LONG_DATA";
		}else if (xmlPath=="finding"){
			return "field_LONG_DATA";
		}else if (xmlPath=="finding/normal_status"){
			return "field_data";
		}else if (xmlPath=="diagnosis"){
			return "field_LONG_DATA";
		}else if (xmlPath=="hippocampal_atrophy"){
			return "field_data";
		}else if (xmlPath=="cortical_atrophy"){
			return "field_data";
		}else if (xmlPath=="leukoaraiosis"){
			return "field_data";
		}else if (xmlPath=="large_infarcts"){
			return "field_data";
		}else if (xmlPath=="small_infarcts"){
			return "field_data";
		}else if (xmlPath=="microbleeds"){
			return "field_data";
		}else if (xmlPath=="site_microbleeds"){
			return "field_data";
		}else if (xmlPath=="lacunes"){
			return "field_data";
		}else if (xmlPath=="other_significant_findings"){
			return "field_data";
		}else if (xmlPath=="aging_changes"){
			return "field_data";
		}else if (xmlPath=="comparison"){
			return "field_data";
		}else if (xmlPath=="history"){
			return "field_data";
		}else if (xmlPath=="exam"){
			return "field_data";
		}else if (xmlPath=="followup_recommended"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<rad:RadiologyRead";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</rad:RadiologyRead>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Modality!=null){
			xmlTxt+="\n<rad:modality";
			xmlTxt+=">";
			xmlTxt+=this.Modality.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:modality>";
		}
		if (this.Reader!=null){
			xmlTxt+="\n<rad:reader";
			xmlTxt+=">";
			xmlTxt+=this.Reader.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:reader>";
		}
		if (this.Technique!=null){
			xmlTxt+="\n<rad:technique";
			xmlTxt+=">";
			xmlTxt+=this.Technique.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:technique>";
		}
		var FindingATT = ""
		if (this.Finding_normalStatus!=null)
			FindingATT+=" normal_status=\"" + this.Finding_normalStatus + "\"";
		if (this.Finding!=null){
			xmlTxt+="\n<rad:finding";
			xmlTxt+=FindingATT;
			xmlTxt+=">";
			xmlTxt+=this.Finding.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:finding>";
		}
		else if(FindingATT!=""){
			xmlTxt+="\n<rad:finding";
			xmlTxt+=FindingATT;
			xmlTxt+="/>";
		}

		if (this.Diagnosis!=null){
			xmlTxt+="\n<rad:diagnosis";
			xmlTxt+=">";
			xmlTxt+=this.Diagnosis.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:diagnosis>";
		}
		if (this.HippocampalAtrophy!=null){
			xmlTxt+="\n<rad:hippocampal_atrophy";
			xmlTxt+=">";
			xmlTxt+=this.HippocampalAtrophy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:hippocampal_atrophy>";
		}
		if (this.CorticalAtrophy!=null){
			xmlTxt+="\n<rad:cortical_atrophy";
			xmlTxt+=">";
			xmlTxt+=this.CorticalAtrophy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:cortical_atrophy>";
		}
		if (this.Leukoaraiosis!=null){
			xmlTxt+="\n<rad:leukoaraiosis";
			xmlTxt+=">";
			xmlTxt+=this.Leukoaraiosis.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:leukoaraiosis>";
		}
		if (this.LargeInfarcts!=null){
			xmlTxt+="\n<rad:large_infarcts";
			xmlTxt+=">";
			xmlTxt+=this.LargeInfarcts;
			xmlTxt+="</rad:large_infarcts>";
		}
		if (this.SmallInfarcts!=null){
			xmlTxt+="\n<rad:small_infarcts";
			xmlTxt+=">";
			xmlTxt+=this.SmallInfarcts;
			xmlTxt+="</rad:small_infarcts>";
		}
		if (this.Microbleeds!=null){
			xmlTxt+="\n<rad:microbleeds";
			xmlTxt+=">";
			xmlTxt+=this.Microbleeds;
			xmlTxt+="</rad:microbleeds>";
		}
		if (this.SiteMicrobleeds!=null){
			xmlTxt+="\n<rad:site_microbleeds";
			xmlTxt+=">";
			xmlTxt+=this.SiteMicrobleeds.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:site_microbleeds>";
		}
		if (this.Lacunes!=null){
			xmlTxt+="\n<rad:lacunes";
			xmlTxt+=">";
			xmlTxt+=this.Lacunes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:lacunes>";
		}
		if (this.OtherSignificantFindings!=null){
			xmlTxt+="\n<rad:other_significant_findings";
			xmlTxt+=">";
			xmlTxt+=this.OtherSignificantFindings;
			xmlTxt+="</rad:other_significant_findings>";
		}
		if (this.AgingChanges!=null){
			xmlTxt+="\n<rad:aging_changes";
			xmlTxt+=">";
			xmlTxt+=this.AgingChanges.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:aging_changes>";
		}
		if (this.Comparison!=null){
			xmlTxt+="\n<rad:comparison";
			xmlTxt+=">";
			xmlTxt+=this.Comparison.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:comparison>";
		}
		if (this.History!=null){
			xmlTxt+="\n<rad:history";
			xmlTxt+=">";
			xmlTxt+=this.History.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:history>";
		}
		if (this.Exam!=null){
			xmlTxt+="\n<rad:exam";
			xmlTxt+=">";
			xmlTxt+=this.Exam.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</rad:exam>";
		}
		if (this.FollowupRecommended!=null){
			xmlTxt+="\n<rad:followup_recommended";
			xmlTxt+=">";
			xmlTxt+=this.FollowupRecommended;
			xmlTxt+="</rad:followup_recommended>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Modality!=null) return true;
		if (this.Reader!=null) return true;
		if (this.Technique!=null) return true;
		if (this.Finding_normalStatus!=null)
			return true;
		if (this.Finding!=null) return true;
		if (this.Diagnosis!=null) return true;
		if (this.HippocampalAtrophy!=null) return true;
		if (this.CorticalAtrophy!=null) return true;
		if (this.Leukoaraiosis!=null) return true;
		if (this.LargeInfarcts!=null) return true;
		if (this.SmallInfarcts!=null) return true;
		if (this.Microbleeds!=null) return true;
		if (this.SiteMicrobleeds!=null) return true;
		if (this.Lacunes!=null) return true;
		if (this.OtherSignificantFindings!=null) return true;
		if (this.AgingChanges!=null) return true;
		if (this.Comparison!=null) return true;
		if (this.History!=null) return true;
		if (this.Exam!=null) return true;
		if (this.FollowupRecommended!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
