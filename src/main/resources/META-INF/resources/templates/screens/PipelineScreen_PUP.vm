$data.setLayoutTemplate("Popup.vm")
##Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
#set ($popup=$data.getParameters().getString("popup"))

<!-- get Javascript Dependencies -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="/scripts/xModal/xModal.js"></script>
<script src="/scripts/pipeline-config-PUP.js"></script>

<!-- ensure popup behavior -->
<script type="text/javascript">
function popup(mylink, windowname) {
    if (! window.focus)return true;
    var href;
    if (typeof(mylink) == 'string')
       href=mylink;
    else
       href=mylink.href;
    window.open(href, windowname, 'width=700,height=500,status=yes,resizable=yes,scrollbars=yes');
    return false;
}
</script>

<link href="$content.getURI("/style/pipemodal.css")" rel="stylesheet" />
<style type="text/css">
    body { margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 100%; background-color: #f3f3f3; }
    .hidden { display: none; }
    .col-2 { width: 45%; float:left; padding-right: 5%; }

    .xmodal fieldset { border-color: #ccc; border-style: solid; border-width: 1px 0 0; margin: 2em 0; padding-top: 10px; }
    .xmodal fieldset legend { border-radius: 3px; padding: 3px; background-color: inherit; }
    .xmodal fieldset label { display: inline-block; font-weight: bold; min-width: 120px; padding-right: 10px;  }
    .xmodal fieldset .col-2 label { min-width: 75px; }

    .xmodal fieldset.error { background-color: #fffcc7; }
    .xmodal fieldset.error legend { color: #c33; }
    .xmodal fieldset.error .required { border-color: #c33; }

    .xmodal input, .xmodal select, .xmodal textarea { border: 1px solid #999; padding: 2px 1px; }

    .xmodal table { border: 1px solid #ccc; }
    .xmodal td, .inputWrapper th { padding: 3px; }
    .xmodal td { border-top: 1px solid #ccc; }
    .xmodal th { text-align: left; background-color: #f3f3f3; }
    tr.empty { background-color: #fffcc7; color: #933; }

	div.xmodal .footer { position: fixed; bottom: 0; right: 0; width: inherit; }
    div.xmodal .footer .buttons .textlink { font-size: 12px; padding: 0 8px; position: relative; top: 5px; }
    div.xmodal.embedded { top: 0; left: 0; border: none; box-shadow: none; display: block; z-index: auto;  }
</style>

<script>
    // set popup window title properly
    $(document).prop("title","Launch PET Pipeline for $pet.getLabel()");
</script>

<!-- create an xmodal container inside the popup window -->
<div id="register_modal" class="pipemodal xmodal fixed embedded" >
    <div class="body content scroll" >
        <div class="inner">
            <!-- modal body start -->

            <!-- handle error messages -->
            #if ($data.message)
            <div class="error">$data.message</div>
            #end

            ## DIAN check for Manual QC
            #if("$project.substring(0,4)"=="DIAN")
                #set($passedQC=false)
                #foreach($assessor in $pet.getAssessors("xnat:qcManualAssessorData"))
                    #if(!$passedQC && "$!assessor.pass"=="1")
                        #set($passedQC=true)
                    #end
                #end
                #if(!$passedQC)
            <div class="error">
            <h3>WARNING</h3>
            Session $pet.getLabel() has not passed Manual QC.
            </div>
                #end
            #end

        #set ($dblQts='"')
        #set ($petScan = "xnat:petScanData")
        #set ($disabled="")

        <form id="pipeline-init" name="PetProcessingPipeline" method="post" action="$link.setAction("BuildAction")" class="optOutOfXnatDefaultFormValidation" >
            <div id="msgDiv"> </div>
                <div id="panel-container">  <!-- this contains all toggle-able panels -->
                    <ul style="display:none"> <!-- this is necessary to initialize jQueryUI Tabs. Users should not see or directly interact with it. Each link corresponds to the ID of a panel that is managed by jQueryUI Tabs.  -->
                        <li><a href="#start">start</a></li>
                        <li><a href="#path1">FS</a></li>
                        <li><a href="#path2">CustomROI</a></li>
                        <li><a href="#path2-confirm">Confirm Custom ROI</a></li>
                        <li><a href="#path3">Template ROI</a></li>
                        <li><a href="#global-params">Global Parameters</a></li>
                        <li><a href="#confirm-params">Confirm Parameters</a></li>
                    </ul>

                    <!-- First Panel: Initialize PUP. Select scan & processing path -->
                    <div id="start">
                        <h2>Initialize PET Unified Pipeline for $pet.getLabel()</h2>
                        <fieldset>
                            <!-- XNAT.data.context parameters -->
                            <input type="hidden" name="sessionId" value="" class="pipeline_input required" />
                            <input type="hidden" name="sessionLabel" value="" class="pipeline_input required" />                        ##  <input type="hidden" name="project" value="" class="pipeline_input required" />
                            <input type="hidden" name="subject" value="" class="pipeline_input" />

                            <legend>Select PET Scan to Process</legend>
                            <select class="pipeline_input required" name="pet_scan_id">
                                <option value="null">SELECT</option>
                            </select>

                            ## get search_element, search_field, search_value (PET Session ID), and project as inputs
                            #xdatPassItemFormFields($search_element $search_field $search_value $project)
                            <input type="hidden" name="pipelineName" value="pet/PETUnifiedPipeline.xml"/>

                            <!-- values passed from PET Session context -->
                            <input type="hidden" name="tracer" value="" class="pipeline_input required" />
                            <input type="hidden" name="format" value="" class="pipeline_input required" />
                            <input type="hidden" name="format_type" value="" class="pipeline_input required" />
                            <input type="hidden" name="half_life" value="" class="pipeline_input required" />
                        </fieldset>
                        <div id="dependent-scan" class="hidden">
                            <div class="col-2">
                                <fieldset>
                                    <legend>Default ROI</legend>
                                    #if ($freesurfers.size() == 0)
                                        <p id="fs-select"><strong>No FreeSurfers Found.</strong> Cannot process using FS for this session.</p>
                                    #else
                                        <p id="fs-select"><label><input type="radio" name="pup-path" value="freesurfer"  checked="checked" /> Use FreeSurfer </label><br /> I will use a Freesurfer assessment from an existing MR session for this subject.</p>
                                    #end
                                </fieldset>
                            </div>
                            <div class="col-2">
                                <fieldset>
                                    <legend>Custom ROI</legend>
                                    <p id="custom-select"><label><input type="radio" name="pup-path" value="customroi" /> Use Uploaded ROI</label><br /> I will use a custom ROI file uploaded for this subject.</p>
                                    <p id="template-select"><label><input type="radio" name="pup-path" value="templateroi" /> Use Template ROI</label><br /> I will use a standard ROI template file. </p>
                                </fieldset>
                            </div>

                            <!-- values set dynamically based on radio selection -->
                            <input type="hidden" name="FS" value="1" class="pipeline_input" />
                            <input type="hidden" name="tgflag" value="0" class="pipeline_input" />
                        </div>
                    </div>

                    <!-- Panel for FreeSurfer Path -->
                    <div id="path1" class="pipeline_path path-1 freesurfer">
                        <h2>Configure FreeSurfer for $pet.getLabel()</h2>
                        <!-- sessions passed through on pipeline launch -->
                        <fieldset class="fs-config" id="fs-select">
                            <legend>Select FreeSurfer Assessor</legend>
                        </fieldset>

                        <input type="hidden" name="mrLabel" value="" class="pipeline_input required" />
                        <input type="hidden" name="mrId" value="" class="pipeline_input required" />
                        <input type="hidden" name="fsversion" value="" class="pipeline_input" />

                        <fieldset class="fs-config">
                            <legend>FreeSurfer Configuration</legend>
                            <p><label for="t1" title="The name of the MR data file in FreeSurfer space">t1</label>
                                <input name="t1" type="text" class="pipeline_input" value="" /></p>
                            <p><label for="wmparc" title="The FreeSurfer segmentation output file">wmparc</label>
                                <input name="wmparc" type="text" class="pipeline_input" value="" /></p>
                            <p><label for="roiimg" title="ROI image file">roiimg</label>
                                <input name="roiimg" type="text" class="pipeline_input" value="" /></p>
                            <p><label for="rsflist" title="RSF list">rsflist</label>
                                <input name="rsflist" type="text" class="pipeline_input" value="" /></p>
                            <p><label for="pvc2cflag" title="Do pvc2c correction">pvc2cflag</label>
                                <input name="pvc2cflag" type="checkbox" class="pipeline_input required" /></p>
                            <p><label for="rsfflag" title="Do RSF correction">rsfflag</label>
                                <input name="rsfflag" type="checkbox" class="pipeline_input required" /></p>
                        </fieldset>

                        <fieldset class="hidden error" id="fs-select-error">
                            <legend>ERROR: NO FREESURFER FOUND</legend>
                            <p>Select a session from this subject and create a FreeSurfer assessor. QC the results and verify it has passed. Then restart this pipeline configuration.</p>
                        </fieldset>

                        <fieldset class="hidden error" id="fs-qc-error">
                            <legend>ERROR: NO FREESURFER HAS PASSED QC</legend>
                            <p>Select a FreeSurfer assessor and examine it. If it passes QC, hit "Edit" on the Actions Box and set QC to Passed. Or run FreeSurfer on any image session on this subject and QC the resulting assessor.
								<br/>Then restart this pipeline configuration.</p>
                        </fieldset>
                    </div>

                    <!-- Paths for Custom ROI -->
                    <div id="path2" class="pipeline_path path-2 customroi">
                        <h2>Select an ROI for $pet.getLabel()</h2>
                        <fieldset id="custom-roi-select">
                            <legend>Select Uploaded ROI from Subject's MR Session</legend>
                            <!-- dynamically populated based on json -->
                        </fieldset>
                        <fieldset class="hidden error" id="custom-roi-select-error">
                            <legend>ERROR: NO ROI FOUND</legend>
                            <p>Select an image session from this subject and upload an ROI file, then restart this pipeline configuration.</p>
                            <!-- dynamically populate list of scans -->
                        </fieldset>
                        <input type="hidden" name="atltarg" class="pipeline_input required" value="" />
                        <input type="hidden" name="mrLabel" class="pipeline_input required" value="" />
                        <input type="hidden" name="mrId" class="pipeline_input required" value="" />
                        <input type="hidden" name="roiimg" class="pipeline_input required" value="" />
                    </div>

                    <div id="path2-confirm" class="pipeline_path path-2 customroi">
                        <h2>Confirm Custom ROI for $pet.getLabel()</h2>
                        <fieldset>
                            <legend>ROI found in ROI Image File</legend>
                            <p><strong>ROI Image File:</strong> <span id="selected-custom-roi"></span></p>
                            <p id="preview-roi-control"><button class="btn" onclick="previewRoi()">Preview ROI</button></p>
                        </fieldset>
                        <fieldset class="hidden" id="preview-roi">
                            <legend>Defined ROI to Include in Build</legend>
                            <table width="90%" id="roi-preview-table">
                                <thead>
                                    <tr><th>Region</th><th>Index</th><th>Voxels</th></tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </fieldset>
                    </div>

                    <!-- Path for Template ROI -->
                    <div id="path3" class="pipeline_path path-3 templateroi">
                        <h2>Select an ROI Template for $pet.getLabel()</h2>
                        <fieldset>
                            <p><label for="template_name">Template ROI File</label>
                                <select name="template_name" class="pipeline_input required" id="roi-template-select">
                                    <option value="null">SELECT</option>
                                </select></p>
                            <p><label for="maskroot">Mask Root</label>
                                <input name="maskroot" type="text" class="pipeline_input required" value="" /></p>
                            <p><label for="roiimg">ROI Image File</label>
                                <input name="roiimg" type="text" class="pipeline_input required" id="roiimg-template-input" value="" /></p>
                        </fieldset>
                    </div>

                    <!-- Global Parameters: view for all pipeline runs -->
                    <div id="global-params" class="global">
                        <h2>Configure Build Parameters for $pet.getLabel()</h2>
                        <fieldset>
                            <legend>Global Pipeline Parameters</legend>
                            <div class="col-2">
                                <p><label for="fwhm" title="Full-width-half-max (fwhm) of the assumed PET scanner point spread function to be used for partial volume correction">fwhm</label>
                                    <input name="fwhm" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="delay" title="delay of scan versus injection (minutes)">delay</label>
                                    <input name="delay" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="tbl" title="Time bin length (seconds)">tbl</label>
                                    <input name="tbl" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="tolmoco" title="Tolerance for motion correction">tolmoco</label>
                                    <input name="tolmoco" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="tolreg" title="Registration tolerance">tolreg</label>
                                    <input name="tolreg" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="refroistr" title="Reference ROI label string">refroistr</label>
                                    <input name="refroistr" type="text" class="pipeline_input required" value="" /></p>
                            </div>
                            <div class="col-2">
                                <p><label for="mst" title="Model Starting Time in Minutes from time of injection and scan for dynamic scans">mst</label>
                                    <input name="mst" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="mdt" title="Model Duration in Minutes for dynamic scans">mdt</label>
                                    <input name="mdt" type="text" class="pipeline_input" value="" /></p>
                                <p><label for="model">model</label>
                                <select name="model" class="pipeline_input">
                                    <option value="null">SELECT</option>
                                </select></p>
                                <p><label for="k2" title="Eflux rate constant for Logan Analysis">k2</label>
                                    <input name="k2" type="text" class="pipeline_input" value="" /></p>
                                <div style="margin-left: 85px;">
                                    <label for="filter" title="Do filtering"><input name="filter" type="checkbox" class="pipeline_input required" />filter</label>
                                        <input type="hidden" name="filterxy" class="pipeline_input required" value="" />
                                        <input type="hidden" name="filterz" class="pipeline_input required" value="" />
                                </div>
                            </div>
                        </fieldset>
                        <fieldset style="background-color: #def">
                            <legend>Advanced Parameters (<a href="javascript:" id="advanced-params-toggle">Show</a>)</legend>
                            <div class="hidden" id="advanced-params">
                                <div class="col-2">
                                    <p><label for="rmf" title="Reference image masking flag for registration">rmf</label>
                                        <input name="rmf" type="text" class="pipeline_input" value="" /></p>
                                    <p><label for="mmf" title="Moving image masking flag for registration">mmf</label>
                                        <input name="mmf" type="text" class="pipeline_input" value="" /></p>
                                    <p><label for="rbf" title="Reference image blurring parameter for registration">rbf</label>
                                        <input name="rbf" type="text" class="pipeline_input" value="" /></p>


                                </div>
                                <div class="col-2">
                                    <p><label for="mbf" title="Moving image blurring parameter for registration">mbf</label>
                                        <input name="mbf" type="text" class="pipeline_input" value="" /></p>
                                    <p><label for="modf" title="Intramodal/intermodal registration flag">modf</label>
                                        <input name="modf" type="text" class="pipeline_input" value="" /></p>
                                    <p><label for="suvr" title="Do SUVR">SUVR</label>
                                        <input name="suvr" type="checkbox" class="pipeline_input required" /></p>
                                    </p>
                                </div>
                            </div>
                        </fieldset>

                    </div>

                    <!-- confirmation of pipeline-specific attributes -->
                    <div id="confirm-params">
                        <h2>Preview Pipeline Parameters for $pet.getLabel()</h2>
                        <!-- list auto-populates with known pipeline parameters with each tab activation -->
                        <table width="90%" id="params-report">
                            <thead>
                                <tr><th>Parameter</th><th>Value</th><th>Required Field?</th></tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div> <!-- end Panel Container -->
                <div class="hidden">
                    <input type="submit" name="eventSubmit_doPup" value="Queue Job" id="submitForm">
                </div>
            </form>

        <!-- modal body end -->
        </div>
    </div>
    <div class="footer" style="background-color: rgb(240, 240, 240); border-color: rgb(224, 224, 224);">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="required_text message content buttons hidden">
                <a id="button-back" class="button" href="javascript:" data-curpanel="0" data-prevpanel="0">Back</a>
            </span>
            <span class="buttons">
                <a class="cancel close textlink" href="javascript:self.close()">Cancel</a>
                <a id="button-advance" class="default button panel-toggle" href="javascript:" data-curpanel="0" data-nextpanel="1">Next Step</a>
            </span>
        </div>
    </div>
</div>
<!-- end of xmodal container -->

<script>
    var paramsObject = $jsonWeNeed;
</script>
