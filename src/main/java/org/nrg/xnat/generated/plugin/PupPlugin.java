package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "nrg_webapp_pup", name = "XNAT 1.7 PUP Plugin", description = "This is the XNAT 1.7 PUP Plugin.",
        dataModels = {@XnatDataModel(value = "pup:pupTimeCourseData",
                singular = "PUP Time Course",
                plural = "PUP Time Courses")})
public class PupPlugin {
}