package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParametersDocument;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;


/**
 * Created by flavin on 6/12/14.
 */
public class PUPLauncher  extends PipelineLauncher{
    static org.apache.log4j.Logger logger = Logger.getLogger(PUPLauncher.class);

    public final String[] stringParams = new String[] {
            "sessionId",
            "sessionLabel",
            "project",
            "subject",
            "mrLabel",
            "mrId",
            "pet_scan_id",
            "tracer",
            "half_life",
            "filterxy",
            "filterz",
            "fwhm",
            "delay",
            "tbl",
            "tolmoco",
            "refroistr",
            "fsid",
            "t1",
            "wmparc",
            "roiimg",
            "rsflist",
            "maskroot",
            "mrscanid",
            "atltarg",
            "tolreg",
            "rmf",
            "mmf",
            "mst",
            "mdt",
            "model",
            "format_type",
            "format",
            "template_name",
            "k2",
            "tgflag"
    };
    public final String[] booleanParams = new String[] {"filter","FS","pvc2cflag","rsfflag","suvr"};
    public final String[] escapedParams = new String[] {"rbf","mbf","modf"};
    public Map<String,String> specialParams;


    public PUPLauncher() {
        // specialParams.put("template-name","template");
    }

    // Need to override this abstract method in PipelineLauncher
    public boolean launch(RunData data, Context context) {
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatImagesessiondata image = new XnatImagesessiondata(data_item);
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, image);

            String pipelineName = (String) TurbineUtils.GetPassedParameter("pipelineName", data);
            xnatPipelineLauncher.setPipelineName(pipelineName);

            String buildDir = PipelineFileUtils.getBuildDir(image.getProject(), true);
            buildDir +=  "pup"  ;

            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            ParametersDocument.Parameters parameters = ParametersDocument.Parameters.Factory.newInstance();
            ParameterData param;

            for (String paramName : stringParams) {
                if (TurbineUtils.HasPassedParameter(paramName, data)) {
                    param = parameters.addNewParameter();
                    param.setName(paramName);
                    param.addNewValues().setUnique((String) TurbineUtils.GetPassedParameter(paramName, data));
                }
            }

            for (String paramName : escapedParams) {
                if (TurbineUtils.HasPassedParameter(paramName, data)) {
                    param = parameters.addNewParameter();
                    param.setName(paramName + "_escaped");

                    String paramWithUnknownEscaping = (String) TurbineUtils.GetPassedParameter(paramName, data);
                    while (paramWithUnknownEscaping.matches(".*&amp;.*") || paramWithUnknownEscaping.matches(".*&quot;.*")) {
                        paramWithUnknownEscaping = paramWithUnknownEscaping.replaceAll("&amp;","&").replaceAll("&quot;", "\"");
                    }
                    param.addNewValues().setUnique(paramWithUnknownEscaping.replaceAll("\"|&#147;|&#148;","&quot;"));
                }
            }

            for (String paramName : booleanParams) {
                param = parameters.addNewParameter();
                param.setName(paramName);
                param.addNewValues().setUnique((TurbineUtils.HasPassedParameter(paramName, data) && TurbineUtils.GetPassedBoolean(paramName,data))? "1" : "0");
            }

            String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            String[] emails = emailsStr.trim().split(",");
            for (String email : emails) {
                if (StringUtils.isNotBlank(email))  xnatPipelineLauncher.notify(email);
            }

            String paramFileName = getName(pipelineName);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir + File.separator + image.getLabel(),paramFileName,parameters);
            xnatPipelineLauncher.setParameterFile(paramFilePath);

            return xnatPipelineLauncher.launch();
        }catch(Exception e) {
            logger.error(e.getCause() + " " + e.getLocalizedMessage());
            return false;
        }
    }
}